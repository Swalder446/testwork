﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp2
{

    public partial class Form1 : Form
    {
        string[] names = new string[10000];

        public Form1()
        {
            InitializeComponent();

            string Filepath = @"SearchParams.txt";
            if (File.Exists(Filepath))
            {
                string[] allStr = File.ReadAllLines(Filepath);
                textBox1.Text = allStr[0];
                textBox2.Text = allStr[1];
                textBox3.Text = allStr[2];
            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath.ToString();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int ii = 0;
            if (textBox1.Text != "")
            {
                string[] allfiles = Directory.GetFiles(@textBox1.Text, "*.txt");

                foreach (string filename in allfiles)
                {
                  if (Convert.ToInt32(label4.Text) == ii)
                    {
                        ii++;
                        label4.Text = ii.ToString();
                    }
                    label7.Text = Path.GetFileName(filename);
                    if (Path.GetFileName(filename).Contains(textBox2.Text))
                    {
                        if (File.ReadAllText(filename).Contains(textBox3.Text))
                        {
                            int i = 0;
                            bool IsInMassive = false;

                            while (names[i] != null)
                            {
                                if (names[i] == filename)
                                    IsInMassive = true;
                                i++;
                            }
                            if (IsInMassive == false)
                            {
                                listBox1.Items.Add(filename);
                                names[i] = filename;
                            }
                        }
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
                button1.Text = "Возобновить";
            }
            else { timer1.Enabled = true; button1.Text = "Приостановить"; }
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox2.Items.AddRange(System.IO.File.ReadAllLines(listBox1.SelectedItem.ToString(), System.Text.Encoding.Default));
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            label4.Text = "0";
            names = new string[10000];
            saveSearch(textBox1.Text, textBox2.Text, textBox3.Text);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            label4.Text = "0";
            names = new string[10000];
            saveSearch(textBox1.Text, textBox2.Text, textBox3.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            label4.Text = "0";
            names = new string[10000];
            saveSearch(textBox1.Text, textBox2.Text, textBox3.Text);
        }

        private void saveSearch(string s1, string s2, string s3)
        {
            string Filepath = @"SearchParams.txt";

            if (s1 == null)
                s1 = "";
            if (s2 == null)
                s2 = "";
            if (s3 == null)
                s3 = "";

            if (File.Exists(Filepath))
                File.Delete(Filepath);

            if (!File.Exists(Filepath))
            {
                using (StreamWriter sw = File.CreateText(Filepath))
                {
                    sw.WriteLine(s1);
                    sw.WriteLine(s2);
                    sw.WriteLine(s3);
                }
            }

        }
    }
}
